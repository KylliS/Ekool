﻿CREATE TABLE [dbo].[Person] (
    [Isikukood] CHAR (11)     NOT NULL,
    [Eesnimi]      NVARCHAR (30) NOT NULL,
    [Klass]     CHAR (5)      NULL,
    [Aine]      CHAR (5)      NULL,
    [Perenimi] NVARCHAR(30) NOT NULL, 
    PRIMARY KEY CLUSTERED ([Isikukood] ASC)
);

